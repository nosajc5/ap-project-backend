const jwt = require('jsonwebtoken');

module.exports = function checkAuth(req, res, next) {
    try {
        const token = req.headers.authorization.split(" ")[1];

        if(token == null) throw new Error();

        const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
        req.userData = decoded;
        next();
    } catch (error) {
        return res.status(401).json({
            message: "Auth failed"
        })
    }
};