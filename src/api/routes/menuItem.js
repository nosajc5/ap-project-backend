const express = require('express');
const router = express.Router();
const menuItemController = require("../../services/menuItem");
const checkAuth =  require("../middleware/checkAuth");

// Create new menu item
router.post("/", menuItemController.add_menu_item);

router.get("/all", menuItemController.get_all_menu_items);

// Get specific menu item
router.get("/item/:menuItemId", menuItemController.get_specific_menu_item);

// Get all menu items in a specified category
router.get("category/:category", menuItemController.get_by_category);

// Get all sales data for a specific menu item
router.get("/item/sales/:menuItemId", menuItemController.sales_menu_item);

// Update menu item
router.patch("/item/:menuItemId", menuItemController.update_menu_item);

// Delete a specific menu item
router.delete("/item/:menuItemId", menuItemController.delete_menu_item);

module.exports = router;