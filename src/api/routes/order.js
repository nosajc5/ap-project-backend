const express = require('express');
const router = express.Router();
const orderController = require("../../services/order");
const checkAuth = require("../middleware/checkAuth");

// Client creates an order
router.post('/', orderController.save_order);

// Getting the specific order details
router.get('/specificOrder/:userID', orderController.get_specific_order);

// Client confirms or updates order
router.patch('/:orderID', orderController.update_order);

// Client cancels order
router.delete('/:orderID', orderController.delete_order);

// Client chooses to view all orders (Manager Only)
router.get('/all', orderController.get_all_orders);

module.exports = router;