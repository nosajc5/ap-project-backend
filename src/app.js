const express = require("express");
const fileUpload = require("express-fileupload");

const app = express();

//  Load environment variables from "./config/.env"
require("dotenv").config({ path: "./config/.env" });

//  Require Routes
const menuItemRoutes = require("./api/routes/menuItem");
const orderRoutes = require("./api/routes/order");
const userRoutes = require("./api/routes/user");


//  Modify response header to allow Cross Origin Access
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "http://localhost:4200");
    res.header("Access-Control-Allow-Headers", 
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === "OPTIONS") {
        res.header("Access-Control-Allow-Methods", "PUT, GET, PATCH, DELETE");
        return res.status(200).json({});
    }
    next();
});

//  Add Middleware
const staticOptions = {
    dotfiles: "ignore",
    index: false,
    redirect: false,
    setHeaders: function(res, path, stat) {
        // Add this header to all static responses
        res.set("x-timestamp", Date.now());
    }
}

app.use("/storage", express.static(`${__dirname}/public`, staticOptions));
app.use(fileUpload());
app.use(express.urlencoded({ extended: false }));
app.use(express.json({ limit: "1mb" }));


//  Request Routes
app.use("/menuItem", menuItemRoutes);
app.use("/order", orderRoutes);
app.use("/user", userRoutes);

//  Home Routes
app.use("/test", (req, res)=> {
    res.status(200).json({
        message: "API working"
    });
});

// Error handling for invalid routes and for 
// errors that may occur in valid routes.
app.use((req, res, next) => {
    const error = new Error("Not found");
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message/*,
            line: error.lineNumber,
            name: error.name,
            fileName: error.fileName,
            stack: error.stack*/
        }
    });
});

module.exports = app;