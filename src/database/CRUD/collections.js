const collection = {
    menuItemsCollection: "MenuItems",
    ordersCollection: "Orders",
    usersCollection: "Users"
}

module.exports = collection;