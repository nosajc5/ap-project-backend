const Firestore = require("../Firebase/Firestore");
const menuItemsCollection = require("./collections").menuItemsCollection;

const db = Firestore.getFirestoreInstance();

async function saveMenuItem(item) {
    let status = false;
    let docRef = db.collection(menuItemsCollection).doc(item.id);

    let setItem =  await docRef.set(item)
        .then(() => {
            status = true;
        })
        .catch(error => {
            console.log("Unable to add menu item: ", error);
            status = false;
        });

    // Return true if item is saved successfully, or false if not
    return status;
}

async function retrieveSpecificMenuItem(id) {
    let item;
    let docRef = db.collection(menuItemsCollection).doc(id);
    let getDoc = await docRef.get()
        .then(doc => {
            if (!doc.exists) {
                console.log("No such document!");
                item = null;
            } else {
                console.log("Document Data:", doc.data());
                item = doc.data();
            }
        })
        .catch(error => {
            console.log("Error getting document", error);
            item = null;
        });

    return item;
}

async function retrieveByCategory(category) {
    let categoryItems = [];
    let docRef = db.collection(menuItemsCollection);
    let queryRef = docRef.where("category", "==", category);
    await queryRef.get()
        .then(snapshot => {
            if (snapshot.empty) {
                //console.log("No matching documents.");
                categoryItems = [];
            }

            categoryItems = snapshot.docs; console.log(categoryItems);
            // snapshot.forEach(doc => {
            //     console.log(doc.id, "=>", doc.data());
            //     categoryItems.push(doc.data());
            // });

        })
        .catch(error => {
            console.log("Error getting documents", error);
            categoryItems = [];
        });
    
    return categoryItems;
}

async function retrieveAllMenuItems() {
    let items = [];

    let menuItemsRef = db.collection(menuItemsCollection);

    let allMenuItems = await menuItemsRef.get()
        .then(snapshot => {
            if(snapshot.docs.length < 1) {
                items = [];
                return;
            }

            snapshot.forEach(doc => { items.push(doc.data()) });
            console.log(items);
        })
        .catch(error => {
            console.log("Error getting documents", error);
            items = [];
        });

    return items;
}

async function updateMenuItem(id, field, value) {
    let status;
    let docRef = db.collection(menuItemsCollection).doc(id);

    let updateDoc = await docRef.update({[field]: value})
        .then(() => status = true)
        .catch(() => status = false);
    
    return status;
}

async function deleteMenuItem(id) {
    let status;
    let deleteDoc = await db.collection(menuItemsCollection).doc(id).delete()
        then(() => {status = true})
        .catch(() => {status = false})

    return status;
}

async function getSales() {

}

module.exports = {
    saveMenuItem,
    retrieveSpecificMenuItem,
    retrieveByCategory,
    retrieveAllMenuItems,
    updateMenuItem,
    deleteMenuItem,
    getSales
};