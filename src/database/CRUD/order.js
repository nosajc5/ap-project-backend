const Firestore = require('../Firebase/Firestore');
const { ordersCollection } = require("./collections");

// Get firestore instance
const db = Firestore.getFirestoreInstance();

//  Save order
async function saveOrder(order) {
    let status = false;
    const docRef = db.collection(ordersCollection).doc(order.id);

    let setOrder = await docRef.set(order)
        .then(() => {
            status = true;
        })
        .catch(error => {
            console.log("Unable to save order ", error);
            status = false;
        })
    
    return status;
}

//  Retrieve a single order from firestore which includes the specified user id
async function retrieveOrder(userID) {
    let order;
    const collectionRef = db.collection(ordersCollection);  // Firestore collection reference
    
    await collectionRef.where("userID", "==", userID).where("status", "==", "unconfirmed").get()    // Add query parameters and execute query
        .then(snapshot => {
            if(snapshot.empty) {    // Checks if the snapshot is empty and return assigns "null" to the order
                console.log("No such document!");
                order = null;
                return;
            }

            order = snapshot.docs[0].data();    // Assigns the first element in the snapshot array to the order variable
        })
        .catch(error => {
            console.log("Error getting document", error);
            order = null;
        })

    return order;   //  Returns the retrieved order or null
}

async function retrieveAllOrders() {
    let orders = []; 
    const ordersRef = db.collection(ordersCollection);  // Firestore collection reference

    let allOrders =  await ordersRef.get()  // Get all documents in the orders collection
        .then(snapshot => {
            if(snapshot.empty) {    // Checks if the snapshot is empty and assign an empty array to the orders array
                console.log("No document found");
                orders = [];
                return;
            }
            
            snapshot.forEach(doc => { orders.push(doc.data()); });  // Iterate over the snapshot array elements and pushes their data portion into the orders arrat
            console.log(orders);
        })
        .catch(error => {
            console.log("Error getting documents", error);
            items = [];
        });

    return orders;  // Returns all the retrieved orders and an empty array
}

async function updateOrder(orderID, field, value) {
    let status = false;
    const docRef = db.collection(ordersCollection).doc(orderID);

    let updateDoc = await docRef.update({[field]: value})
        .then(() => { status = true })
        .catch(() => { status = false });

    return status;
}

async function deleteOrder(orderID) {
    let status = false;
    const deleteDoc = await db.collection(ordersCollection).doc(orderID).delete()
        .then(() => { status = true })
        .catch(() => { status = false });

    return status;
}

module.exports = {
    saveOrder,
    retrieveOrder,
    retrieveAllOrders,
    updateOrder,
    deleteOrder
};