const Firestore = require("../Firebase/Firestore");
const usersCollection = require("./collections").usersCollection;

const db = Firestore.getFirestoreInstance();

async function addUser(user) {
    let status = false;
    let docRef = db.collection(usersCollection).doc(user.id);

    await docRef.set(user)
        .then(() => {
            status = true;
        })
        .catch(error => {
            console.log("Unable to add user ", error);
            status = false;
        });

    return status;
}

async function getUser({ firstName, lastName }) {
    let user;
    let usersRef = db.collection(usersCollection);
    let queryRef = usersRef.where('firstName', '==', firstName).where('lastName', '==', lastName);
    
    await queryRef.get()
        .then(snapshot => {
            if(snapshot.empty) {
                user = null;
                return;
            }
            
            user = snapshot.docs[0].data();
        })
        .catch(error => {
            user = null;
            console.log("Error");
        });
    
    console.log(user);
    return user;
}

module.exports = {
    addUser,
    getUser,
};