class Firebase {
    constructor() {

    }

    static _getFirebaseInstance() {
        if (Firebase.admin == undefined) {
            const setup = require("../../util/initializeFirebase");
            setup.initializeFirebase();
            Firebase.admin = setup.admin;
            return Firebase.admin;
        }
        return Firebase.admin;
    }
}

module.exports = Firebase;