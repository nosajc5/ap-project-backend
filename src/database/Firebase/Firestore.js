const Firebase = require("./Firebase");

class Firestore extends Firebase {
    constructor() {
        super();
    }

    static getFirestoreInstance() {
        if (Firestore.db == undefined) {
            const admin = super._getFirebaseInstance();
            Firestore.db = admin.firestore();
            return Firestore.db;
        }
        return Firestore.db;
    }
}

module.exports = Firestore;