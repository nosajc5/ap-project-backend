const userCRUD = require("./CRUD/user");
const menuItemCRUD = require("./CRUD/menuItem");
const orderCRUD = require("./CRUD/order");

module.exports = class Database {
    constructor() {}

    //  User CRUD methods
    addUser(parameter) { return userCRUD.addUser(parameter); }
    getUser(parameter) { return userCRUD.getUser(parameter); }

    // Menu Items CRUD methods
    addMenuItem(item) { return menuItemCRUD.saveMenuItem(item); }
    getSpecificMenuItem(itemId) { return menuItemCRUD.getSpecificMenuItem(itemId); }
    retrieveByCategory(category) { return menuItemCRUD.retrieveByCategory(category); }
    retrieveByAllMenuItems() { return menuItemCRUD.retrieveAllMenuItems(); }
    updateMenuItem(id, field, value) { return menuItemCRUD.updateMenuItem(id, field, value); }
    deleteMenuItem(itemId) { return menuItemCRUD.deleteMenuItem(itemId); }

    //  Order CRUD methods
    saveOrder(order) { return orderCRUD.saveOrder(order); }
    retrieveOrder(id) { return orderCRUD.retrieveOrder(id); }
    retrieveAllOrder() { return orderCRUD.retrieveAllOrders(); }
    updateOrder(id, field, value) { return orderCRUD.updateOrder(id, field, value); }
    deleteOrder(id) { return orderCRUD.deleteOrder(); }
}