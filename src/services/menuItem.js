const Datebase = require("../database"),
    db = new Datebase();
const fileStoragePath = process.env.FILE_STORAGE;
const photoStoragePath = process.env.PHOTO_FILE_STORAGE;
const aslStoragePath = process.env.ASL_FILE_STORAGE;
const englishAudioStoragePath = process.env.ENGLISH_AUDIO_STORAGE;

exports.add_menu_item = (req, res, next) => {
    console.log(req.body);
    console.log(req.files);
    const generateMenuItemId = require("../util/generateMenuItemId");
    const getFileExtension = require("../util/getFileExtension");

    //  Destructure menu item properties from request body
    const { name, category, description, stockQuantity, unit, cost } = req.body;

    //  Destructure menu item media files from request files
    const { photo, aslRepresentation, englishAudioRepresentation } = req.files;

    //  Check if all required menu item properties are present in request
    if(!(name && category && description && stockQuantity && unit && cost))
        res.status(400).json({
            message: "Unable to add item: all properties are required",
            requiredProperties: ["name", "category", "description", "stockQuantity", "unit", "cost"]
        });

    if(!(photo && aslRepresentation && englishAudioRepresentation))
        res.status(400).json({
            message: "Unable to add item: all media properties are required",
            requiredProperties: ["photo", "aslRepresentation", "englishAudioRepresentation"]
        });

    //  Generate unqiue menu item identifier
    const id = generateMenuItemId(name);

    //  Generate unique filename from random id and file extension
    const photoFilename = id + getFileExtension(photo.name);
    const aslRepFilename = id + getFileExtension(aslRepresentation.name);
    const englishAudioRepFilename = id + getFileExtension(englishAudioRepresentation.name);

    const item = {
        id: id,
        name,
        category,
        description,
        stockQuantity,
        unit,
        cost,
        photo: {
            name: photoFilename,
            path: `files/productImages/${photoFilename}`,
            md5Hash: photo.md5,
        },
        aslRep: {
            name: aslRepFilename,
            path: `files/aslRep/${aslRepFilename}`,
            md5Hash: aslRepresentation.md5
        },
        englishAudioRep: {
            name: englishAudioRepFilename,
            path: `files/englishAudioRep/${englishAudioRepFilename}`,
            md5Hash: englishAudioRepresentation.md5
        }
    }

    //  Add menu item to database and save files
    db.addMenuItem(item)
        .then(status => {
            if(status) {
                photo.mv(`${photoStoragePath}/${photoFilename}`);
                aslRepresentation.mv(`${aslStoragePath}/${aslRepFilename}`);
                englishAudioRepresentation.mv(`${englishAudioStoragePath}/${englishAudioRepFilename}`);

                return res.status(201).json({
                    message: "Menu Item Added"
                });
            }
            else
                throw new Error("Unable to add menu item");
        })
        .catch(error => {
            console.log(error);
            return res.status(500).json({
                message: error.message
            });
        });
}

exports.get_all_menu_items = (req, res, next) => {
    db.retrieveByAllMenuItems()
        .then(result => {
            if(result.length < 1)
                return res.status(204).json({
                    message: "No menu item stored"
                });

            res.status(200).json(result);
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({
                message: "Unable to retrieve menu items"
            });
        });
}

exports.get_specific_menu_item = (req, res, next) => {
    const id = req.params.menuItemId;

    db.getSpecificMenuItem(id)
        .then(result => {
            if(result === null)
                return res.status(404).json({
                    message: "Menu item not found"
                });

            return res.status(200).json(result);        
        })
        .catch(error => {
            console.log(error);
            return res.status(500).json({
                message: "Unable to get menu item"
            });
        });
}

exports.get_by_category = (req, res, next) => {
    const category = req.params.category;

    db.retrieveByCategory(category)
        .then(result => {
            if(result.length < 1)
                return res.status(404).json({
                    message: "No menu item found for specified category"
                });

            return res.status(200).json(result);
        })
        .catch(error => {
            console.log(error);
            return res.status(500).json({
                message: "Unable to menu items"
            });
        });
}

exports.update_menu_item = (req, res, next) => {
    console.log(req.params, req.body);
    const id = req.params.menuItemId;
    const { key, value } = req.body;

    if(key !== "cost" && key !== "stockQuantity")
        res.status(400).json({
            message: "Menu item property to update invalid: see options",
            options: ["cost", "stockQuantity"]
        });
        
    if(value == undefined || typeof value !== "number")
        res.status(400).json({
            message: "Value to update menu item property with is required (in number)"
        });    

    db.updateMenuItem(id, key, value)
        .then(status => {
            if(status == false)
                throw new Error();

            return res.status(200).json({
                message: "Menu item updated"
            });
        })
        .catch(() => {
            return res.status(500).json({
                message: "Unable to update menu item",
            });
        });
}

exports.delete_menu_item = (req, res, next) => {
    const id = req.params.menuItemId;

    db.deleteMenuItem(id)
        .then(status => {
            if(status == false)
                throw new Error("Unable to delete menu item");

            return res.status(200).json({
                message: "Menu item deleted"
            });
        })
        .catch((error) => {
            return res.status(500).json({
                message: error.message
            });
        });
}

exports.sales_menu_item = (req, res, next) => {
    
}
