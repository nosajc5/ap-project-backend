const Database = require("../database"),
    db = new Database();

exports.save_order = (req, res, next) => {
    const { customerId, items } = req.body;

    if(!(customerId, items))
        res.status(400).json({
            message: "Unable to add item: all properties are required",
            requiredProperties: ["customerId", "items"]
        });

    db.getUser(customerId)
        .then(user => {
            if(user == null)
                throw new Error();

            const customerName = user.firstName;
            const amtItems = items.length;

            const orderItems = [];
            let totalCost = 0;

            let promises = [];

            items.forEach(item => {
                promises.push(db.getSpecificMenuItem(item.id));
            })

            Promise.all(promises)
                .then(menuItems => {
                    
                    items.forEach((item, index) => {
                        orderItems.push({
                            id: item.id,
                            quantity: item.quantity,
                            cost: menuItems[index].cost
                        });

                        totalCost += result.cost;
                    });

                    const generateOrderID = require("../util/generateOrderID")
                    const generatedOrderId = generateOrderID();

                    const order = {
                        id: generatedOrderId,
                        customerId,
                        customerName,
                        orderItems,
                        amtItems,
                        totalCost,
                        status: "unconfirmed"
                    };
                
                    db.saveOrder(order)
                        .then(status => {
                            if(status == true) {
                                console.log("Order saved", order.id);
                                res.status(201).json({
                                    message: `Order saved: ${order.id}`
                                });
                            }
                            else {
                                throw new Error("Failed to save order to database");
                            }
                        })
                        .catch(error => {
                            throw error;
                        });

                })   
                .catch(error => {
                    console.log("Unable to save order", error);
                    res.status(500).json({
                        message: "Unable to save order"
                    });
                });
        })
        .catch(error => {
            console.log("Unable to save order: ", error);
            res.status()
        });

}

exports.get_all_orders = (req, res, next) => {

    db.retrieveAllOrder()
        .then(result => {
            if(result.length < 1)
                return res.status(204).json({
                    message: "No order/s stored"
                });

            res.status(200).json(result);
        })
        .catch(error => {
            console.log(error);
            res.status(500).json({
                message: "Unable to retrieve orders"
            });
        });
}

exports.get_specific_order = (req, res, next) => {
    const { userID } = req.params;

    if(!userID)
        res.status(400).json({
            message: "No search parameter specified",
            requiredParameter: "userID"
        });

    db.retrieveOrder(userID)
        .then(result => {
            if(result === null)
                return res.status(404).json({
                    message: "Order not found"
                });

            return res.status(200).json(result);
        })
        .catch(error => {
            console.log(error);
            return res.status(500).json({
                message: "Unable to get order"
            });
        });
}

exports.update_order = (req, res, next) => {
    const { orderID } = req.params;

    if(!orderID)
        res.status(400).json({
            message: "No search parameter specified",
            requiredParameter: "orderID"
        });

    db.updateOrder(orderID, "status", "confirmed")
        .then(status => {
            if(status == false)
                throw new Error();

            return res.status(200).json({
                message: "Order updated"
            });
        })
        .catch(error => {
            console.log(error);
            return res.status(500).json({
                message: "Unable to update order"
            });
        });
}

exports.delete_order = (req, res, next) => {
    console.log(req.params);
    const { orderID } = req.params;

    if(!orderID)
        res.status(400).json({
            message: "No search parameter specified",
            requiredParameter: "orderID"
        });

    db.deleteOrder(orderID)
        .then(status => {
            if(status == false) 
                throw new Error();

            return res.status(200).json({
                message: "Order deleted"
            });
        })
        .catch(error => {
            console.log(error);
            return res.status(500).json({
                message: "Unable to delete order"
            });
        });
}