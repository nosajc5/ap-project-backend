const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const Database = require("../database"),
    db = new Database();
const generateUserID = require("../util/generateUserID");
const checkDigitalIdTypeValid = require("../util/checkDigitalIdTypeValid");

exports.user_register = (req, res, next) => {
    const { firstName, lastName, idType } = req.body;

    if(checkDigitalIdTypeValid(idType) === false) 
        res.status(406).json({
            message: "Digital ID type invalid",
            idTypes: ["Password", "SignatureMove", "VoiceRecording"]
        });

    const id = generateUserID({firstName, lastName});
    let user = {
        id,
        firstName,
        lastName,
        role: 'C'
    }

    switch (idType.toString()) {
        case "Password" || "password":
            const { password } = req.body;

            bcrypt.hash(password, 10)
                .then(async function(hash) {
                    let successful;

                    await db.addUser({
                        ...user,
                        idType: "Password",
                        password: hash
                    })
                    .then(value => {successful = value})
                    .catch(error => {throw new Error(error.message)})

                    if(successful == true) {
                        res.status(201).json({message: "User Created"});
                        console.log(`User created: user`, user);
                    }
                    else {
                        throw new Error("Unable to create user");
                    }
                })
                .catch(err => {
                    res.status(500).json({message: err.message});
                });

            break;
    
        default:
            break;
    }
}

exports.user_login = (req, res, next) => {
    const { firstName, lastName, idType } = req.body;
    
    if(checkDigitalIdTypeValid(idType) === false) 
        res.status(406).json({
            message: "Digital ID type invalid",
            idTypes: ["Password", "SignatureMove", "VoiceRecording"]
        });

    db.getUser({firstName, lastName})
        .then(userFromDb => {
            
            if(userFromDb === null)
                return res.status(404).json({
                    message: "User not found"
                });
    
            // Compares the user's Digital id type from the the database to the API request
            if(userFromDb.idType != idType)
                return res.status(409).json({
                    message: "Digital ID Mismatch"
                });
        
            // Switch idType to select connect auth id
            switch (idType) {
                case "Password" || "password":
                    const password = req.body.password;
                    const passwordFromDb  = userFromDb.password;
            
                    bcrypt.compare(password, passwordFromDb)
                        .then(match => {
                            const user = { 
                                id: userFromDb.id,
                                firstName: userFromDb.firstName,
                                lastName: userFromDb.lastName,
                                role: userFromDb.role
                            }
        
                            if(match) {
                                const token = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET);
                    
                                return res.status(200).json({
                                    message: "Auth successful",
                                    ...user,
                                    accessToken: token
                                });
                            }
                            else {
                                return res.status(401).json({
                                    message: "Auth failed: - Invalid Password"
                                });
                            }
                        })
                        .catch(error => {
                             return res.status(500).json({
                                message: "Auth failed",
                                error: error.message
                            });
                        }); 
        
                    break;
            
                default:
                    break;
            }
        })
        .catch(() => {
            return res.status(500).json({
                message: "Auth failed"
            });
        });
}