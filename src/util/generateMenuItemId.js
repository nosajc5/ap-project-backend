module.exports = function generateMenuItemId(name) {
    const randomBytes = require("crypto").randomBytes(6).toString("hex");
    return `${name}_${randomBytes}`;
}