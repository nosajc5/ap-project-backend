module.exports = function generateUserID({ firstName, lastName }) {
    const firstNameFormatted = firstName.charAt(0).toUpperCase() + firstName.slice(1);
    const lastNameFormatted = lastName.charAt(0).toUpperCase() + lastName.slice(1);
    return `${firstNameFormatted}_${lastName}`;
}