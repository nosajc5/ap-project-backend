module.exports = function getFileExtension(fileName) {
    const split = fileName.split(".");  //  Split filename string at '.'
    const index = split.length - 1;     //  Index of last element
    const extension = split[index];     //  Assign last element as the extension

    return `.${extension}`; //  Concatenate extension to a dot ('.')
}