// Firebase setup
const admin = require("firebase-admin");
const serviceAccount = require("../config/serviceAccountKey.json");

function initializeFirebase() {
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount),
        databaseURL: "https://advanced-programming-pro.firebaseio.com"
    });
}

module.exports = { admin, initializeFirebase };